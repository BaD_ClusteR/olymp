<html>
    <head>
        <title>Test server</title>
        <script src="/articles/jquery.js"></script>
        <link rel="stylesheet" href="/articles/style.css" />
    </head>
    <body>
        <?php if (empty($template) || $template != "user"):?>
            <div class="left">
                <span>Личный кабинет</span>
                <form method="GET" action="/login" id="form">
                    <div class="error"></div>
                    <label for="login">Логин: </label><input type="text" name="login" id="login" value="" />
                    <label for="password">Пароль: </label><input type="password" name="password" id="password" value="" />
                    <input type="submit" value="Зайти" />
                </form>
            </div>
            <script>
                $("#form").on("submit", function() {
                    $(".error").removeClass("visible");
                    $.ajax("/login", {
                        type: "GET",
                        dataType: "json",
                        data: {
                            login: $("#login").val(),
                            pass: $("#password").val()
                        },
                        success: function(res) {
                            if (res.result !== "error")
                                window.location = "/user";
                            else {
                                $(".error").html(res.text).addClass("visible");
                            }
                        }
                    });
                    return false;
                });
            </script>
        <?php else :?>
            <div class="left">
                <span>Привет, <strong><?=$_SESSION['user2']['login']?>!</strong></span>
                <a style="display:block;margin:10px 0 20px 0" href="/logout">Выйти</a>
                <span>Поменять регистрационные данные</span>
                <form method="POST" id="form">
                    <div class="error"></div>
                    <label for="login">Логин: </label><input type="text" id="login" name="login" value="<?=$_SESSION['user2']['login']?>" />
                    <label for="password">Пароль: </label><input type="password" id="password" value="<?=$_SESSION['user2']['password']?>" />
                    <input type="submit" value="Поменять" />
                </form>
                <script>
                    $("#form").on("submit", function() {
                        $.ajax("/user/change", {
                            type: "POST",
                            dataType: "json",
                            success: function(data) {
                                if (!data.result) {
                                    $(".error").html(data.text);
                                } else {
                                    window.location.reload();
                                }
                            }
                        });
                        return false;
                    })
                </script>
            </div>
        <?php endif; ?>
        <div class="content">
            <?php include __DIR__ . "/static/" . (empty($template) ? "404" : $template) . ".php"; ?>
        </div>
    </body>
</html>