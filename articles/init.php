<?php

    session_start();

    $path = explode("/", explode("?", $_SERVER['REQUEST_URI'])[0]);
    $template = "404";
    define("SALT", "m9JNEM{9vk#Gmj{{PASS}}lvEaFQeNpUhZF@CkU");

    if (!empty($path[1])) {
        if ($path[1] == "login") {
            /** MySQL settings */
            define("DB_HOST", "localhost");
            define("DB_USER", "root");
            define("DB_PASS", "");
            define("DB_DATABASE", "articles");

            function login($login, $password) {
                $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
                //$password = md5(str_replace("{{PASS}}", $password, SALT));
                $sql = "SELECT * FROM users WHERE login = '$login' AND password = '$password'";
                $res = $db->query($sql);
                $user = ($res ? $res->fetch_assoc() : []);
                if (!empty($user) && !empty($user['id']))
                    $_SESSION['user2'] = $user;
                $result = (!empty($user) && !empty($user['id']) ? [ 'result' => true ] : [
                    'result' => false,
                    'sql' => $sql,
                    'error' => $db->error
                ]);
                $db->close();
                return $result;
            }

            $login = (!empty($_GET['login']) ? $_GET['login'] : "");
            $pass = (!empty($_GET['pass']) ? $_GET['pass'] : "");

            $result = ['result' => "OK", 'text' => "", 'debug' => ""];
            $loginResult = login($login, $pass);
            if (!$loginResult['result']) {
                $result = [
                    'result' => "error",
                    'text' => "Неправильный логин и/или пароль.",
                    'debug' => $loginResult['error'] . "\n\n" . $loginResult['sql']
                ];
            }
            header("Content-Type:application/json");
            die(json_encode($result));
        } elseif ($path[1] == "article") {
            if (sizeof($path) === 3 && preg_match("/^[A-Za-z0-9_-]+$/", $path[2])
                && strlen($path[2]) < 16
                && (!in_array(strtolower($path[2]), ["user", "404", "403", "index"]))
                && file_exists(__DIR__ . "/static/" . $path[2] . ".php")
            ) {
                $template = $path[2];
            }
        } elseif ($path[1] == "user") {
            if (!empty($_SESSION['user2']) && !empty($_SESSION['user2']['id'])) {
                if (sizeof($path) > 2) {
                    if (sizeof($path) == 3 && $path[2] == "change") {
                        die(json_encode(['result' => false, 'text' => "Ошибка записи в базу данных!"]));
                    }
                    header("HTTP/1.1 302 Found");
                    header("Location: /user");
                    die();
                }
                $template = "user";
            } else {
                $template = "403";
            }
        } elseif ($path[1] == "logout" && sizeof($path) == 2) {
            unset($_SESSION['user2']);
            header("HTTP/1.1 302 Found");
            header("Location: /");
            die();
        }
    } else {
        $template = "index";
    }

    switch ($template) {
        case "403": header("HTTP/1.1 403 Access Denied"); break;
        case "404": header("HTTP/1.1 404 Not Found"); break;
    }
    include "article.php";
