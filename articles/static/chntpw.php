<h1>chntpw - utility to overwrite Windows NT_2000</h1>
<b>NAME</b><pre>       chntpw - utility to overwrite Windows NT/2000 SAM passwords

</pre><h4 id="synopsis"><b>SYNOPSIS</b></h4><pre>       <b>chntpw</b> [<u>options</u>] &lt;<u>samfile</u>&gt; [<u>systemfile</u>] [<u>securityfile</u>] [<u>otherreghive</u>] [...]

</pre><h4 id="description"><b>DESCRIPTION</b></h4><pre>       This  manual  page documents briefly the <b>chntpw</b> command.  This manual page was written for
       the Debian distribution because the original program does not have a manual page.

       <b>chntpw</b> is a utility to view some information  and  change  user  passwords  in  a  Windows
       NT/2000  SAM  userdatabase  file,  usually  located at \WINDOWS\system32\config\SAM on the
       Windows file system. It is not necessary to know the old  passwords  to  reset  them.   In
       addition  it contains a simple registry editor (same size data writes) and hex-editor with
       which the information contained in a registry file can be browsed and modified.

</pre><h4 id="options"><b>OPTIONS</b></h4><pre>       <b>-h</b>     Show summary of options.

       <b>-u</b> <b>username</b>
              Username to change. Default is Administrator

       <b>-l</b>     List all users in the SAM database.

       <b>-i</b>     Interactive: list all users (as per -l) and then ask for the user to change.

       <b>-e</b>     Registry editor with limited capabilities.

       <b>-d</b>     Use buffer debugger.

       <b>-t</b>     Show hexdumps of structs/segments (deprecated debug function).

</pre><h4 id="examples"><b>EXAMPLES</b></h4><pre>       <b>ntfs-3g</b> <b>/dev/sda1</b> <b>/media/win</b> <b>;</b> <b>cd</b> <b>/media/win/WINDOWS/system32/config/</b>
              Mount the Windows file system and  enters  the  directory  <b>\WINDOWS\system32\config</b>
              where Windows stores the SAM database.

       <b>chntpw</b> <b>SAM</b> <b>system</b>
              Opens  registry  hives  <b>SAM</b>  and <b>system</b> and change administrator account. This will
              work even if the name has been changed or it has been  localized  (since  different
              language versions of NT use different administrator names).

       <b>chntpw</b> <b>-l</b> <b>SAM</b>
              Lists the users defined in the <b>SAM</b> registry file.

       <b>chntpw</b> <b>-u</b> <b>jabbathehutt</b> <b>SAM</b>
              Prompts  for  password for <b>jabbathehutt</b> and changes it in the <b>SAM</b> registry file, if
              found (otherwise do nothing).

</pre><h4 id="see also"><b>SEE</b> <b>ALSO</b></h4><pre>       If you are looking for an automated procedure for password recovery, you might look at the
       bootdisks provided by the upstream author at <b><a href="">http://pogostick.net/~pnh/ntpasswd/</a></b>
       There  is  more  information  on how this program works available at <u>/usr/share/doc/chntpw</u>
       registry works.

</pre><h4 id="author"><b>AUTHOR</b></h4><pre>       This manual page was written by  Javier  Fernandez-Sanguino  &lt;<a href="">jfs@computer.org</a>&gt;,  for  the
       Debian GNU/Linux system (but may be used by others).

                                         13th March 2010                                CHNTPW(8)
</pre>
<p>После этого вы увидите меню, в котором сможете редактировать, удалить пароль или заблокировать пользователя.</p>
<p><span style="color: #333399;"><em>Примечание: Информация для исследования, обучения или проведения аудита. Применение в корыстных целях карается законодательством РФ.</em></span></p>