<h1>Сервер со статьями</h1>
<p>Последние созданные статьи:</p>
<p><a href="/article/john">John the Ripper в составе Kali Linux</a></p>
<p><a href="/article/uuid">Как решить ошибку "UUID уже сущечтвует" в VirtualBox</a></p>
<p><a href="/article/vhd">Как открыть Windows, установленную на виртуальном VHD-диске, в VirtualBox</a></p>
<p><a href="/article/injection">SQL injection для начинающих</a></p>
<p><a href="/article/injections">SQL инъекции. Проверка, взлом, защита</a></p>
<p><a href="/article/chntpw">chntpw - utility to overwrite Windows NT_2000</a></p>
<p><a href="/article/sampasswd">sampasswd - reset passwords of users in the SAM user database</a></p>