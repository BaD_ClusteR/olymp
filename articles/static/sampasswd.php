<h1>sampasswd - reset passwords of users in the SAM user database</h1>
<b>NAME</b><pre>sampasswd - reset passwords of users in the SAM user database
</pre><h4 id="synopsis"><b>SYNOPSIS</b></h4><pre>       <b>sampasswd</b> [<u>options</u>] -u<u>user</u> &lt;<u>samfile</u>&gt;
</pre><h4 id="description"><b>DESCRIPTION</b></h4><pre>       This  manual  page  documents briefly the <b>sampasswd</b> command.  This manual page was written
       for the Debian distribution because the original program does not have a manual page.
       <b>sampasswd</b> is a non-interactive command line utility  that  can  reset  a  user's  password
       and/or  the  user's  account  bits  from the SAM user database file of a Microsoft Windows
       system (Windows NT, 2000, XP, Vista, 7, 8.1, etc.).   This  file  is  usually  located  at
       \WINDOWS\system32\config\SAM on the file system of a Microsoft Windows Operating System
       On success, the program does not output any informatin and the exit code is 0.
</pre><h4 id="options"><b>OPTIONS</b></h4><pre>       <b>-h</b>     Show summary of options.
       <b>-r</b>     Reset the user's password.
       <b>-a</b>     Reset  all  the  users. If this option is used there is no need to specify the next
              option.
       <b>-u</b> <b>&lt;user&gt;</b>
              User to change. The user value can be provided as a username, or a  RID  number  in
              hexadecimal   (if   the  username  is  preceded  with  '0x').  Usernames  including
              international characters will probably not work.

       <b>-l</b>     Lists the users in the SAM database.

       <b>-H</b>     Output human readable output. The program by default will print  a  parsable  table
              unless this option is used.

       <b>-N</b>     Do  not  allocate  more information, only allow the editing of existing values with
              same size.

       <b>-E</b>     Do not expand the hive file (safe mode).

       <b>-t</b>     Print debug information of allocated blocks.

       <b>-v</b>     Print verbose information and debug messages.

</pre><h4 id="known bugs"><b>KNOWN</b> <b>BUGS</b></h4><pre>       If the username  includes  international  (non-ASCII)  characters  the  program  will  not
       (usually) find it. Use the RID number instead.

</pre><h4 id="see also"><b>SEE</b> <b>ALSO</b></h4><pre>       <b>chntpwd,</b> <b>reged,</b> <b>samusrgrp</b>
       You  will  find   more  information available on how this program works, in the text files
       <u>/usr/share/doc/chntpw/README.txt</u> and <u>/usr/share/doc/chntpw/MANUAL.txt</u>

       More    documentation    is    available    at     the     upstream's     author     site:
       <b><a href="">http://pogostick.net/~pnh/ntpasswd/</a></b>

</pre><h4 id="author"><b>AUTHOR</b></h4><pre>       This program was written by Petter N Hagen.

       This manual page was written by Javier Fernandez-Sanguino &lt;<a href="">jfs@debian.org</a>&gt;, for the Debian
       GNU/Linux system (but may be used by others).

                                         6th August 2014                             <a href="">SAMPASSWD</a>(8)
</pre>
<p>После этого вы увидите меню, в котором сможете редактировать, удалить пароль или заблокировать пользователя.</p>
<p><span style="color: #333399;"><em>Примечание: Информация для исследования, обучения или проведения аудита. Применение в корыстных целях карается законодательством РФ.</em></span></p>