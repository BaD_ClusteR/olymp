<?php

    define("DEBUG_MODE", true);

    /** MySQL settings */
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASS", "");
    define("DB_DATABASE", "olymp");
    define("DB_CHARSET", "utf8");


    define("ROOT_DIR", dirname(__DIR__));
    define("INCLUDE_DIR", ROOT_DIR . "/include");
    define("CLASSES_DIR", INCLUDE_DIR . "/classes");
    define("TPL_DIR", ROOT_DIR . "/tpl");

    define("PASS_SALT", "m9JNEM{9vk#Gm#Xj{{PASS}}lvEaFQ\$eNpUhZF@C");
    date_default_timezone_set("Europe/Samara");


    $BC = [
        'dirs' => [
            'assets' => "/tpl/assets",
            'js' => "/tpl/assets/js",
            'css' => "/tpl/assets/css"
        ]
    ];


    include_once "load.php";