<?php

    if (!BUser::getInstance()->logged() || empty($_SESSION['user']['isAdmin'])) {
        header("302 Found");
        header("Location: /");
        die();
    } else {
        $content = "users";
        $title = "Admin zone";
        $vars = [];
        $columns = [];
        if (empty($path[1])) {
            $vars = BDashboard::getDashboardInfo(BDashboard::BDASHBOARD_SORT_LOGIN);
            foreach ($vars as &$var) {
                $var['first_login'] = empty($var['first_login']) ? "----" : explode(" ", $var['first_login'])[1];
                $var['points'] .= '<br /><span class="subinfo">Fine: ' . $var['time_fine'] . '</span>';
            }
            $columns = [
                'id'          => "ID",
                'login'       => "Login",
                'ip'          => "IP",
                'first_login' => "First login",
                'points'      => "Points"
            ];
            $title = "Users";

        } else if ($path[1] == "attempts") {
            $title = "Attempts";
            $vars = BDashboard::getAttempts();
            foreach ($vars as &$var) {
                $var['dt'] = explode(" ", $var['dt'])[1];
                $var['status_text'] = ($var['status'] == "S" ? "Success" : "Fail");
                $var['@class'] = ($var['status'] == "S" ? "success" : "fail");
            }
            $columns = [
                'dt'          => "Time",
                'login'       => "Login",
                'number'      => "Task number",
                'answer'      => "Answer",
                'status_text' => "Status"
            ];
        } else if ($path[1] == "commands") {
            $title = "Command log";
            $vars = BDashboard::getCommands();
            foreach ($vars as &$var) {
                $var['dt'] = explode(" ", $var['dt'])[1];
                $var['cmd'] = "<code>" . $var['cmd'] . "</code>";
            }
            $columns = [
                'dt'    => "Time",
                'login' => "Login",
                'ip'    => "IP",
                'cmd'   => "Command"
            ];
        } else if ($path[1] == "logoff") {
            BUser::getInstance()->logoff();
            header("302 Found");
            header("Location: /");
            die();
        }

        $this->render("admin/main", [
            'title'   => $title,
            'content' => "table",
            'vars'    => [
                'title'   => $title,
                'data'    => $vars,
                'columns' => $columns
            ]
        ]);
    }