<?php

    class BAjax {

        private $path = null;

        private function returnJSON($arr) {
            header("Content-Type:application/json");
            die(json_encode($arr));
        }

        private function error($text = "Unknown error") {
            header("HTTP/1.1 400 Bad Request");
            $this->returnJSON([ 'error' => strval($text) ]);
        }


        private function genLeaf($user, $task) {
            $db = BQuery::getInstance();
            $isSolved = !empty($db->getFirstRow(
                "SELECT id FROM attempts 
                      WHERE user_id = :user AND status = 'S' AND task_id = :task
                            AND dt >= :start AND dt <= :end
                    ",
                ['user' => $user, 'task' => $task, 'start' => System::getOlympStartTime(), 'end' => System::getOlympEndTime()]
            )['id']);
            $result = [
                'name' => ($task == 1 ? "p{$user}t1" : "t$task"),
                'solved' => $isSolved,
                'user' => $user,
                'task' => $task
            ];
            if ($task < TASKS_COUNT)
                $result['children'] = [ $this->genLeaf($user, $task + 1) ];
            else
                $result['size'] = 1;
            return $result;
        }

        public function __construct($path) {
            $this->path = $path;
            $isAdmin = BUser::getInstance()->logged() && !empty($_SESSION['user']['isAdmin']);
            $db = BQuery::getInstance();
            define("TASKS_COUNT", $db->getFirstRow("SELECT COUNT(*) AS count FROM tasks")['count']);
            $users = BDashboard::getDashboardInfo(BDashboard::BDASHBOARD_SORT_NAME);

            if ($this->path[0] == "board") {
                $result = [
                    'name' => "root",
                    'children' => []
                ];
                for ($i = 0; $i < sizeof($users); $i++)
                    $result['children'][] = $this->genLeaf($users[$i]['id'], 1);
                $this->returnJSON($result);
            } elseif ($this->path[0] == "answers") {
                $attempts = BDashboard::getAttempts(200, null, (empty($this->path[1]) ? null : intval($this->path[1])));
                $result = [];
                foreach ($attempts as $attempt) {
                    if ($attempt['status'] == "S") {
                        $result[] = [
                            'id' => $attempt['id'],
                            'user' => $attempt['user_id'],
                            'dt' => $attempt['dt'],
                            'number' => $attempt['number']
                        ];
                    }
                }
                $this->returnJSON($result);
            } elseif ($this->path[0] == "board_table") {
                $this->returnJSON(BDashboard::getDashboardTableInfo());
            } elseif ($this->path[0] == "init") {
                $result = BBash::getInstance()->welcomeScreen(true);
                if (!BUser::getInstance()->logged()) {
                    if (empty($_SESSION['login']))
                        $result[] = ["setPrompt", "hal9000 login: "];
                    else {
                        $result[] = ["setPrompt", "Password for $_SESSION[login]: "];
                        $result[] = ["setMask", "*"];
                    }
                }
                $this->returnJSON(['status' => "success", 'cmds' => $result]);
            } elseif ($isAdmin && $this->path[0] == "attempts") {
                $start_id = (empty($this->path[1]) ? 0 : intval($this->path[1]));
                $result = BQuery::getInstance()->getResults(
                    "SELECT a.id, t.number, a.answer, a.status, a.dt, u.login, u.name, u.ip
	                  FROM attempts a
		                LEFT JOIN tasks t ON (a.task_id = t.id)
		                LEFT JOIN users u ON (a.user_id = u.id)
	                  WHERE a.id > ?", [$start_id]);
                foreach ($result as &$row) {
                    $row['dt'] = explode(" ", $row['dt'])[1];
                    $row['status_text'] = ($row['status'] == "S" ? "Success" : "Fail");
                    $row['@class'] = ($row['status'] == "S" ? "success" : "fail");
                }
                $this->returnJSON($result);
            } elseif ($isAdmin && $this->path[0] == "commands") {
                $userid = (!empty($_GET['user']) ? intval($_GET['user']) : null);
                $start_id = (!empty($this->path[1]) ? intval($this->path[1]) : null);
                $vars = BDashboard::getCommands(200, $userid, $start_id);
                $result = [];
                foreach ($vars as $var) {
                    $result[] = [
                        'id' => $var['id'],
                        'ip' => $var['ip'],
                        'dt' => explode(" ", $var['dt'])[1],
                        'cmd' => "<code>" . $var['cmd'] . "</code>",
                        'login' => htmlspecialchars($var['login'])
                    ];
                }
                $this->returnJSON($result);
            } else
                $this->error();
        }

    }