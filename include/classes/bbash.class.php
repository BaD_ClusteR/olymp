<?php

    define("WELCOME_MSG", "");

    class BBash {

        private static $instance = null;

        private function returnJSON($commands) {
            header("Content-Type:application/json");
            die(json_encode([
                'status' => "success",
                'cmds' => $commands
            ]));
        }

        private function JSONError($text) {
            header("Content-Type:application/json");
            die(json_encode([
                'status' => "error",
                'error' => $text
            ]));
        }

        private function isWhite($c) {
            return (in_array($c, [" ", "\t", "\n"]));
        }

        private function skipWhites($cmd, &$i = 0) {
            $len = mb_strlen($cmd);
            while ($i < $len && $this->isWhite($cmd[$i]))
                $i++;
        }

        private function printTemplate($name, $vars = []) {
            $template = BQuery::getInstance()->getFirstRow("SELECT template FROM msg_templates WHERE name = ?", $name);
            $text = System::format($template['template'], $vars);
            $result = [];
            foreach (explode("\n", $text) as $str)
                $result[] = ["echo", (!empty($str) ? $str : " ")];
            return $result;
        }

        private function getToken($cmd, &$i = 0) {
            $len = strlen($cmd);
            if ($i >= $len)
                return "";
            $this->skipWhites($cmd, $i);
            if ($i >= $len)
                return "";
            $stopSign = " ";
            if (in_array($cmd[$i], ["'", '"'])) {
                $stopSign = $cmd[$i];
                $i++;
            }
            $result = "";
            while ($i < $len) {
                if ($stopSign == " " && $this->isWhite($cmd[$i]) || $cmd[$i] == $stopSign) {
                    $i++;
                    break;
                } else {
                    $result .= $cmd[$i];
                }
                $i++;
            }
            return $result;
        }

        private function parse($cmd) {
            $i = 0;
            $len = strlen($cmd);
            $tokens = [];
            while ($i < $len) {
                $token = $this->getToken($cmd, $i);
                if (!empty($token))
                    $tokens[] = $token;
            }
            return $tokens;
        }

        private function log($cmd) {
            $user = BUser::getInstance();
            $query = BQuery::getInstance();
            $query->insert("commands", [
                'dt' => date(System::DT_FORMAT),
                'user_id' => ($user->logged() ? $user->getID() : null),
                'cmd' => $cmd,
                'ip' => $_SERVER['REMOTE_ADDR']
            ]);
            return $query->lastInsertID();
        }

        public function welcomeScreen($asVar = false) {
            $user = BUser::getInstance();
            $result = [];
            if (!$user->logged()) {
                $result = $this->printTemplate("logo");
            } else {
                $olymp_end = System::getOlympEndTime(true);
                $ts = time();
                $timeColor = "#FFF";
                if ($olymp_end - $ts < 600)
                    $timeColor = "#FF3737";
                elseif ($olymp_end - $ts < 3600)
                    $timeColor = "#FBF309";
                $vars = [
                    'USERS_ONLINE' => BUser::getInstance()->getLoggedUsersCount(),
                    'PROCESSES' => 150 + mt_rand(-47, 47),
                    'IP' => $_SERVER['REMOTE_ADDR'],
                    'TIME' => System::getTimeToEnd(),
                    'PLACE' => BUser::getInstance()->getCurrentPlace(),
                    'PLACES' => BDashboard::getParticipantsCount(),
                    'TIME_COLOR' => $timeColor
                ];
                $result = $this->printTemplate("welcome", $vars);
                if (!System::hasOlympStarted())
                    array_splice($result, sizeof($result) - 4, 1,
                        $this->printTemplate("server_diag", [
                            'TIME_START' => date("H:i", System::getOlympStartTime(true)),
                            'TIME_END' => date("H:i", System::getOlympEndTime(true)),
                            'TIME' => date("H:i:s")
                        ])
                    );
                elseif (System::hasOlympEnded()) {
                    $result[sizeof($result) - 4][1] =
                        "                                             " .
                        substr($result[sizeof($result) - 4][1], 75);
                    array_splice($result, sizeof($result) - 3, 0,
                        $this->printTemplate("olymp_ended", [
                            'TIME_END' => date("H:i", System::getOlympEndTime(true)),
                            'TIME' => date("H:i:s")
                        ])
                    );
                }
                if (System::isOlympGoing()) {
                    if (empty($user->getMeta("mail1"))) {
                        $mails = 1;
                        if (!empty($user->getMeta("mail2_got")) && empty($user->getMeta("mail2"))) {
                            $mails ++;
                        }
                        if ($mails == 1) {
                            $result[] = ["echo", "У Вас одно непрочитанное письмо."];
                        } elseif ($mails == 2) {
                            $result[] = ["echo", "У Вас два непрочитанных письма."];
                        }
                    } elseif (!empty($user->getMeta("mail2_got")) && empty($user->getMeta("mail2"))) {
                        $result[] = ["echo", "У Вас одно непрочитанное письмо."];
                    }
                }
            }
            if ($asVar)
                return $result;
            else
                $this->returnJSON($result);
            return true;
        }

        private function mailList() {
            $result = [];
            $mail1 = "";
            $mail2 = "";
            $user = BUser::getInstance();
            if (empty($user->getMeta("mail1")))
                $mail1 .= "[[b;#FFF;]";
            $mail1 .= "1. ". System::getOption("mail1_date") . "    От: Big Boss    Тема: Задание на завтра";
            if (empty($user->getMeta("mail1")))
                $mail1 .= "]";
            if (!empty($user->getMeta("mail2_got"))) {
                if (empty($user->getMeta("mail2")))
                    $mail2 .= "[[b;#FFF;]";
                $mail2 .= "2. " . $user->getMeta("mail2_got") . "    От: Тринити     Тема: Проверь, пожалуйста...";
                if (empty($user->getMeta("mail2")))
                    $mail2 .= "]";
            }
            $result[] = ["echo", $mail1];
            if (!empty($mail2))
                $result[] = ["echo", $mail2];
            return $result;
        }

        private function readMail($index) {
            $user = BUser::getInstance();
            if ($index == 1) {
                $user->setMeta("mail1", time());
                return $this->printTemplate("mail1", ['LAST_LOGIN' => System::getOption("mail1_date"), 'END_TIME' => date("H:i", System::getOlympEndTime(true))]);
            } elseif ($index == 2 && !empty(BUser::getInstance()->getMeta("mail2_got"))) {
                $user->setMeta("mail2", time());
                return $this->printTemplate("mail2", [ 'MAIL_TIME' => $user->getMeta("mail2_got") ]);
            }

            return [["echo", "Нет письма с таким индексом."]];
        }

        private function checkOlympTime() {
            $result = System::isOlympGoing();
            if (!$result)
                $this->returnJSON($this->printTemplate(System::hasOlympStarted() ? "diag_olymp_ended" : "diag"));
            return $result;
        }

        private function _echo(&$exec, $text) {
            $exec[] = ["echo", $text];
        }

        private function checkIfLocked() {
            $db = BQuery::getInstance();
            $user = BUser::getInstance();
            $lock = $db->getFirstRow("SELECT * FROM locks WHERE ip = ? AND end_time >= ? ORDER BY end_time DESC", [
                $_SERVER['REMOTE_ADDR'], date(System::DT_FORMAT)
            ]);
            if (!empty($lock))
                $this->returnJSON($this->printTemplate("ip_blocked", [
                    'TIME_TO_END' => System::diffTime(time(), $lock['end_time'])
                ]));
            if ($user->logged()) {
                $lock = $db->getFirstRow("SELECT * FROM locks WHERE user_id = ? AND end_time >= ? ORDER BY end_time DESC", [
                    $user->getID(), date(System::DT_FORMAT)
                ]);
                if (!empty($lock))
                    $this->returnJSON($this->printTemplate("login_blocked", [
                        'TIME_TO_END' => System::diffTime(time(), $lock['end_time'])
                    ]));
            }
        }

        private function afterTaskHook($number) {
            $user = BUser::getInstance();
            $result = [];
            if ($number == 1) {
                $user->setMeta("mail2_got", date(System::DT_FORMAT));
                $this->_echo($result, "У вас новое входящее письмо.");
            }
            return $result;
        }

        public function exec($cmd) {
            $user = BUser::getInstance();
            $db = BQuery::getInstance();
            $exec = [];
            $executed = false;
            $this->checkIfLocked();
            if (empty($_SESSION['user'])) {
                if (empty($_SESSION['login'])) {
                    $this->log("<b>Login:</b> " . htmlspecialchars($cmd));
                    $_SESSION['login'] = $cmd;
                    $exec[] = ["setPrompt", "Password: "];
                    $exec[] = ["setMask", "*"];
                } else {
                    if ($user->auth($_SESSION['login'], $cmd)) {
                        $this->log("<b>Password for $_SESSION[login]:</b> <span style='color:green;'>" . htmlspecialchars($cmd) . "</span>");
                        if ($_SESSION['user']['isAdmin']) {
                            $exec[] = ["redirect", "/admin"];
                        } else {
                            $lastLogin = (empty($_SESSION['user']['last_login']) ?
                                System::getOption("last_login") : $_SESSION['user']['last_login']
                            );
                            $exec = array_merge($exec, $this->printTemplate("welcome_first",
                                [ 'LOGIN' => $_SESSION['user']['login'], 'LAST_LOGIN' => $lastLogin ]
                            ));
                            $exec = array_merge($exec, $this->welcomeScreen(true));
                            $exec[] = ["setMask"];
                            $exec[] = ["setPrompt", "[[;#0CD62E;]" . $_SESSION['user']['login'] . "@hal9000][[;#0317FF;]~]\$ "];
                        }
                    } else {
                        $this->log("<b>Password for $_SESSION[login]:</b> <span style='color:red;'>" . htmlspecialchars($cmd) . "</span>");
                        $exec[] = ["echo", "Неверная пара логин/пароль."];
                        $exec[] = ["setMask"];
                        $exec[] = ["setPrompt", "hal9000 login: "];
                    }
                    unset($_SESSION['login']);
                }
            } else {
                $terminalName = $user->getTerminalName();
                if (!$user->checkFingerprint()) {
                    $exec[] = ["echo", System::format("Похоже, что отпечаток Вашего браузера поменялся, {{0}}. Для безопасности Вы были логоффнуты.", $terminalName)];
                    $user->logoff();
                    $exec[] = ["setPrompt", "hal9000 login: "];
                } else if (trim($cmd) != "" && strlen($cmd) < 256) {
                    $this->log(htmlspecialchars($cmd));
                    $tokens = $this->parse($cmd);
                    switch ($tokens[0]) {
                        case "status":
                            $exec = array_merge($exec, [["cls"]], $this->welcomeScreen(true));
                            break;
                        case "cls":
                            $exec = array_merge($exec, [["cls"]]);
                            break;
                        case "help":
                            $exec = array_merge([["cls"]], $this->printTemplate("help"));
                            break;
                        case "dashboard":
                            $exec = array_merge($exec, [["open", "/dashboard"]]);
                            break;
                        case "sha1":
                            if ($this->checkOlympTime())
                                $this->_echo($exec, sha1(empty($tokens[1]) ? "" : strval($tokens[1])));
                            break;
                        case "task":
                            $this->checkOlympTime();
                            $result = System::answer(empty($tokens[1]) ? "" : $tokens[1]);
                            $this->_echo($exec, $result['text']);
                            if ($result['code'] == 1)
                                $exec = array_merge($exec, $this->afterTaskHook($result['number']));
                            break;
                        case "mail":
                            $this->checkOlympTime();
                            $exec = array_merge($exec, empty($tokens[1]) ? $this->mailList() : $this->readMail($tokens[1]));
                            break;
                        case "logout":
                            $exec = array_merge(
                                $this->printTemplate("logout", ['USER_LOGIN' => $_SESSION['user']['login']]),
                                [["setPrompt", "hal9000 login: "]]
                            );
                            $user->logoff();
                            break;
                    }
                } elseif ($cmd != "")
                    $this->_echo($exec, "Терминал не обрабатывает команды длиннее 256 символов.");
            }
            if (empty($exec) && !$executed)
                $exec[] = ["echo", "Неизвестная команда: $cmd"];

            System::maybeLock();

            $this->returnJSON($exec);
        }

        public function init() {
            if (empty($_SESSION['id'])) {
                if (empty($_SESSION['login'])) {
                    $result = [ 'text' => "", 'prompt' => "hal9000 login: ", 'mask' => "" ];
                } else {
                    $result = [ 'text' => "Пароль для пользователя " . $_SESSION['login'], 'prompt' => "Password: ", 'mask' => "*" ];
                }
            } else {
                $result = [ 'text' => "Hello, " . $_SESSION['user']['login'] . ".", 'prompt' => "[[;#0CD62E;]" . $_SESSION['user']['login'] . "@hal9000][[;#0317FF;]~]$ ", 'mask' => "" ];
            }
            return $result;
        }

        public static function getInstance() {
            if (self::$instance == null)
                self::$instance = new self();
            return self::$instance;
        }

    }