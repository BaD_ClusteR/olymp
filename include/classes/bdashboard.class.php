<?php

    class BDashboard {

        const BDASHBOARD_SORT_POINTS = 1;
        const BDASHBOARD_SORT_LOGIN = 2;
        const BDASHBOARD_SORT_NAME = 3;

        public static function getParticipants() {
            return BQuery::getInstance()->getResults("SELECT * FROM users WHERE isAdmin = 0");
        }

        public static function getParticipantsCount() {
            return BQuery::getInstance()->getFirstRow("SELECT COUNT(*) AS count FROM users WHERE isAdmin = 0")['count'];
        }

        public static function getDashboardInfo($sortType = self::BDASHBOARD_SORT_POINTS) {
            $users = self::getParticipants();
            foreach ($users as &$user)
                $user = array_merge($user, BUser::getInstance()->getPoints($user['id']));
            $sortFunc = null;
            switch ($sortType) {
                case self::BDASHBOARD_SORT_POINTS: $sortFunc = function($a, $b) {
                    if ($a['points'] < $b['points'])
                        return 1;
                    elseif ($a['points'] > $b['points'])
                        return -1;
                    elseif ($a['time_fine'] > $b['time_fine'])
                        return -1;
                    elseif ($a['time_fine'] < $b['time_fine'])
                        return 1;
                    elseif ($a['surname'] < $b['surname'])
                        return -1;
                    elseif ($a['surname'] > $b['surname'])
                        return 1;
                    elseif ($a['name'] < $b['name'])
                        return -1;
                    elseif ($a['name'] > $b['name'])
                        return 1;
                    elseif ($a['login'] < $b['login'])
                        return 1;
                    elseif ($a['login'] > $b['login'])
                        return 1;
                    else
                        return 0;
                }; break;
                case self::BDASHBOARD_SORT_LOGIN: $sortFunc = function($a, $b) {
                    if ($a['login'] < $b['login'])
                        return -1;
                    elseif ($a['login'] > $b['login'])
                        return 1;
                    return 0;
                }; break;
                case self::BDASHBOARD_SORT_NAME: $sortFunc = function($a, $b) {
                    if ($a['surname'] < $b['surname'])
                        return -1;
                    elseif ($a['surname'] > $b['surname'])
                        return 1;
                    elseif ($a['name'] < $b['name'])
                        return -1;
                    elseif ($a['name'] > $b['name'])
                        return 1;
                    elseif ($a['login'] < $b['login'])
                        return -1;
                    elseif ($a['login'] > $b['login'])
                        return 1;
                    return 0;
                };
            }
            if (!empty($sortFunc))
                usort($users, $sortFunc);
            return $users;
        }

        public static function getDashboardTableInfo() {
            $data = BDashboard::getDashboardInfo(BDashboard::BDASHBOARD_SORT_POINTS);
            $_user = BUser::getInstance();
            $result = [];
            foreach ($data as $user) {
                $result[] = [
                    'id' => $user['id'],
                    'name' => $user['surname'] . " " . ($user['name']) . (!empty($user['patronym']) ? " $user[patronym]" : ""),
                    'points' => $user['points'],
                    'fine' => $user['time_fine'],
                    'solved' => $_user->getSolvedTasksCount($user['id']),
                    'current' => $_user->logged() && $_user->getID() == $user['id']
                ];
            }
            return $result;
        }

        public static function getAttempts($maxCount = 20, $userID = null, $startID = null) {
            $maxCount = min(200, max(1, $maxCount));
            $olymp_start = strtotime(System::getOption("time_start"));
            $olymp_end = strtotime(System::getOption("time_end"));
            $sql = "SELECT a.id, a.user_id, t.number, a.answer, a.status, a.dt, u.login, u.name, u.ip  
                    FROM attempts a
                        LEFT JOIN tasks t ON (a.task_id = t.id)
		                LEFT JOIN users u ON (a.user_id = u.id)
                    WHERE dt >= :dt_start AND dt <= :dt_end";
            $vars = [
                'max_count' => $maxCount,
                'dt_start' => date(System::DT_FORMAT, $olymp_start),
                'dt_end' => date(System::DT_FORMAT, $olymp_end)
            ];
            if (!empty($userID)) {
                $sql .= " AND u.id = :user_id";
                $vars['user_id'] = $userID;
            }
            if (!empty($startID)) {
                $sql .= " AND a.id > :start_id";
                $vars['start_id'] = $startID;
            }
            $sql .= " ORDER BY dt DESC LIMIT :max_count";
            return BQuery::getInstance()->getResults($sql, $vars);
        }

        public static function getCommands($maxCount = 20, $userID = null, $startID = null) {
            $maxCount = min(200, max(1, $maxCount));
            $sql = "SELECT u.login, u.name, c.* 
                    FROM commands c
		                LEFT JOIN users u ON (c.user_id = u.id)
                    WHERE 1";
            $vars = [
                'max_count' => $maxCount,
            ];
            if (!empty($userID)) {
                $sql .= " AND u.id = :user_id";
                $vars['user_id'] = $userID;
            }
            if (!empty($startID)) {
                $sql .= " AND c.id > :start_id";
                $vars['start_id'] = $startID;
            }
            $sql .= " ORDER BY dt DESC LIMIT :max_count";
            return BQuery::getInstance()->getResults($sql, $vars);
        }

    }