<?php

    class BPath {

        private $allowed_roots = [ "dashboard", "admin", "cmd", "ajax" ];

        private static $instance = null;

        public function render($template, $vars = []) {
            global $BC;

            foreach ($vars as $varName => $var)
                $$varName = $var;
            include TPL_DIR . "/" . $template . ".tpl.php";
        }

        public function __construct() {
            $path = (empty($_SERVER['REQUEST_URI']) ? "/" : $_SERVER['REQUEST_URI']);
            $path = explode("?", $path)[0];
            $path = explode("/", $path);
            if (sizeof($path) > 1 && empty($path[0]))
                array_shift($path);
            if (empty($path))
                $path = [ "" ];
            if (empty($path[0])) {
                $init = BBash::getInstance()->init();
                $this->render("console", ['init' => $init]);
                die();
            } else if (!in_array($path[0], $this->allowed_roots)) {
                header("302 Found");
                header("Location: /");
                die();
            } else if ($path[0] == "admin") {
                include INCLUDE_DIR . "/admin.php";
            } else if ($path[0] == "cmd") {
                $cmd = (!empty($_POST['cmd']) ? $_POST['cmd'] : "");
                if (DEBUG_MODE && empty($cmd) && !empty($_GET['cmd']))
                    $cmd = $_GET['cmd'];
                BBash::getInstance()->exec($cmd);
            } else if ($path[0] == "dashboard") {
                $this->render("board", [
                    'info' => BDashboard::getDashboardInfo(BDashboard::BDASHBOARD_SORT_NAME),
                    'table' => BDashboard::getDashboardTableInfo()
                ]);
            } else if ($path[0] == "ajax") {
                array_shift($path);
                new BAjax($path);
            } else {
                header("302 Found");
                header("Location: /");
                die();
                //die($path[0]);
            }
        }

        public static function getInstance() {
            if (self::$instance == null)
                self::$instance = new self();
            return self::$instance;
        }

    }