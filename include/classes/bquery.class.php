<?php

    class BQuery {

        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $db = DB_DATABASE;
        private $charset = DB_CHARSET;

        private $handler = null;

        private static $instance = null;

        const BQUERY_ASSOC = 1;
        const BQUERY_NUM = 2;
        const BQUERY_OBJ = 3;

        private function connect() {
            $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
            $pdo_options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false
            ];
            $result = new PDO($dsn, $this->user, $this->pass, $pdo_options);
            return $result;
        }

        private function performRequest($sql, $vars) {
            if (is_scalar($vars))
                $vars = [$vars];
            $stmt = $this->handler->prepare($sql);
            $stmt->execute($vars);
            return $stmt;
        }

        private function getSetSql($vars) {
            $result = "";
            foreach ($vars as $index => $var) {
                $varName = str_replace("`", "``", $index);
                $result .= ($result == "" ? "" : ", ") . "`$varName` = :$varName";
           }
           if ($result != "")
               $result = " SET " . $result;
           return $result;
        }

        public function __construct() {
            $this->handler = $this->connect();
        }

        public function getResults($sql, $vars = [], $resultType = self::BQUERY_ASSOC) {
            $res = $this->performRequest($sql, $vars);
            $resultType = ($resultType == self::BQUERY_ASSOC ? PDO::FETCH_ASSOC :
                ($resultType == self::BQUERY_NUM ? PDO::FETCH_NUM : PDO::FETCH_OBJ)
            );
            return $res->fetchAll($resultType);
        }

        public function getFirstRow($sql, $vars = [], $resultType = self::BQUERY_ASSOC) {
            $res = $this->performRequest($sql, $vars);
            $resultType = ($resultType == self::BQUERY_ASSOC ? PDO::FETCH_ASSOC :
                ($resultType == self::BQUERY_NUM ? PDO::FETCH_NUM : PDO::FETCH_OBJ)
            );
            return $res->fetch($resultType);
        }

        public function getColumn($sql, $vars = [], $column = 0) {
            $res = $this->performRequest($sql, $vars);
            return $res->fetchColumn($column);
        }

        public function insert($tableName, $vars = []) {
            $tableName = str_replace("`", "``", $tableName);
            $sql = "INSERT INTO `$tableName`" . $this->getSetSql($vars);
            $this->performRequest($sql, $vars);
            return $this->handler->lastInsertId();
        }

        public function update($tableName, $vars = [], $whereSQL = "", $whereVars = []) {
            if (empty($vars))
                return null;
            $tableName = str_replace("`", "``", $tableName);
            $sql = "UPDATE `$tableName`" . $this->getSetSql($vars) .
                   (!empty($whereSQL) ? " WHERE $whereSQL" : "");
            $res = $this->performRequest($sql, array_merge($vars, $whereVars));
            return $res->fetchAll(PDO::FETCH_ASSOC);
        }

        public function lastInsertID() {
            $result = $this->getFirstRow("SELECT LAST_INSERT_ID() AS id");
            return (!empty($result['id']) ? $result['id'] : null);
        }

        public static function getInstance() {
            if (self::$instance === null)
                self::$instance = new self();
            return self::$instance;
        }

    }