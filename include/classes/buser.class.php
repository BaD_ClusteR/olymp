<?php

    class BUser {

        private $db = null;

        private static $instance = null;

        private function generatePasswordHash($pass) {
            return sha1(str_replace("{{PASS}}", $pass, PASS_SALT));
        }

        private function getFingerprint() {
            $result = "";
            if (!empty($_SERVER['REMOTE_ADDR']))
                $result .= $_SERVER['REMOTE_ADDR'];
            if (!empty($_SERVER['HTTP_USER_AGENT']))
                $result = $_SERVER['HTTP_USER_AGENT'];
            return md5($result);
        }

        public function auth($login, $pass) {
            $user = $this->db->getFirstRow("SELECT * FROM users WHERE login = ? AND password = ?",
                [ $login, $this->generatePasswordHash($pass) ]
            );
            if (empty($user))
                return false;
            $_SESSION['user'] = $user;
            $_SESSION['id'] = $user['id'];
            $_SESSION['user']['fp'] = $this->getFingerprint();
            $this->db->update("users", ['logged' => 0], "ip = :ip OR login = :login", ['ip' => $_SERVER['REMOTE_ADDR'], 'login' => $user['login']]);
            $toUpdate = [ 'last_login' => date(System::DT_FORMAT), 'ip' => $_SERVER['REMOTE_ADDR'], 'logged' => 1 ];
            if (empty($user['first_login']))
                $toUpdate['first_login'] = $toUpdate['last_login'];
            $this->db->update("users", $toUpdate, "id = :id", ['id' => $user['id']]);
            return true;
        }

        public function logged() {
            return (!empty($_SESSION['id']) && !empty($_SESSION['user']) && is_array($_SESSION['user']));
        }

        public function getID() {
            return ($this->logged() ? intval($_SESSION['id']) : null);
        }

        public function logoff() {
            unset($_SESSION['user']);
            unset($_SESSION['id']);
            $this->db->update("users", [ 'logged' => 0 ]);
            return true;
        }

        public function addUser($login, $pass, $meta = []) {
            if (!empty($this->db->getFirstRow("SELECT login FROM users WHERE login = ?", [ $login ])))
                return false;
            $meta['login'] = $login;
            $meta['password'] = $this->generatePasswordHash($pass);
            return $this->db->insert("users", $meta);
        }

        public function checkFingerprint() {
            if ($this->logged() && $_SESSION['user']['fp'] != $this->getFingerprint()) {
                unset($_SESSION['user']);
                unset($_SESSION['id']);
                return false;
            }
            return true;
        }

        public function getTerminalName() {
            return (!$this->logged() ? "Guest" : $_SESSION['user']['login']);
        }

        public function getPoints($id = null) {
            $result = [
                'points' => 0,
                'time_fine' => 0
            ];
            if (empty($id) && $this->logged())
                $id = $this->getID();
            if (!empty($id)) {
                $olymp_start = strtotime(System::getOption("time_start"));
                $olymp_end = strtotime(System::getOption("time_end"));
                $answers = [];
                $answersList = $this->db->getResults(
                    "SELECT task_id, a.answer, status, dt, points 
                        FROM attempts a
                        LEFT JOIN tasks t ON (a.task_id = t.id)
                        WHERE user_id = ? AND dt >= ? AND dt <= ? AND status = 'S'
                        ORDER BY dt",
                    [$id, date(System::DT_FORMAT, $olymp_start), date(System::DT_FORMAT, $olymp_end)]
                );
                foreach ($answersList as $answer) {
                    if (empty($answers[$answer['task_id']])) {
                        $answers[$answer['task_id']] = true;
                        $result['points'] += $answer['points'];
                        $result['time_fine'] += (strtotime($answer['dt']) - $olymp_start);
                    }
                }
            }
            return $result;
        }

        public function getLoggedUsersCount() {
            return $this->db->getFirstRow("SELECT COUNT(*) AS count FROM users WHERE logged = 1")['count'];
        }

        public function getCurrentPlace() {
            if (!$this->logged() || !empty($_SESSION['user']['isAdmin']))
                return null;
            $dbinfo = BDashboard::getDashboardInfo();
            foreach ($dbinfo as $index => $user)
                if ($user['login'] == $_SESSION['user']['login'])
                    return $index + 1;
                return null;
        }

        public function getMeta($name) {
            if (!$this->logged())
                return null;
            $allMeta = $this->db->getFirstRow("SELECT meta FROM users WHERE id = ?", $this->getID());
            if (empty($allMeta['meta']))
                return null;
            $allMeta = json_decode($allMeta['meta'], true);
            return (!empty($allMeta[$name]) ? $allMeta[$name] : null);
        }

        public function setMeta($name, $value) {
            if (!$this->logged())
                return null;
            $allMeta = $this->db->getFirstRow("SELECT meta FROM users WHERE id = ?", $this->getID());
            $allMeta = (empty($allMeta['meta']) ? [] : json_decode($allMeta['meta'], true));
            $allMeta[$name] = $value;
            $this->db->update("users", ['meta' => json_encode($allMeta)], "id = :id", ['id' => $this->getID()]);
            return true;
        }

        public function addFromCSV($filename) {
            $db = BQuery::getInstance();
            $f = fopen($filename, "rt");
            if (!$f)
                return false;
            $id = 2;
            while (!feof($f)) {
                $exists = !empty($db->getFirstRow("SELECT id FROM users WHERE id = ?", $id)['id']);
                $result = fgetcsv($f);
                $item = [
                    'login' => $result[3],
                    'password' => $this->generatePasswordHash($result[4]),
                    'last_login' => null,
                    'first_login' => null,
                    'logged' => 0,
                    'ip' => null,
                    'isAdmin' => 0,
                    'meta' => "",
                    'name' => $result[0],
                    'surname' => $result[1],
                    'patronym' => $result[2]
                ];
                if (!$exists)
                    $db->insert("users", array_merge($item, [ 'id' => $id ]));
                else
                    $db->update("users", $item, "id = :id", ['id' => $id]);
                $id++;
            }
            return true;
        }

        public function getSolvedTasksCount($userID = null) {
            if (empty($userID) && !$this->logged())
                return 0;
            if (empty($userID))
                $userID = $this->getID();
            return BQuery::getInstance()->getFirstRow(
                "SELECT COUNT(distinct(task_id)) AS count FROM attempts WHERE user_id = :id AND status = 'S'
                  AND dt >= :start AND dt <= :end",
                [
                    'id' => $userID,
                    'start' => System::getOlympStartTime(),
                    'end' => System::getOlympEndTime()
                ]
            )['count'];
        }

        public static function getInstance() {
            if (self::$instance === null)
                self::$instance = new self();
            return self::$instance;
        }

        public function __construct() {
            $this->db = BQuery::getInstance();
        }
    }