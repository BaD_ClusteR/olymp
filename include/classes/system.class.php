<?php

    class System {

        const DT_FORMAT = "Y-m-d H:i:s";

        public static function format($str, $vars = []) {
            $from = [];
            $to = [];
            if (!is_array($vars)) {
                $from[] = "{{0}}";
                $to[] = strval($vars);
            } else {
                foreach ($vars as $key => $value) {
                    $from[] = "{{$key}}";
                    $to[] = $value;
                }
            }
            return str_replace($from, $to, $str);
        }

        public static function getOption($name) {
            $res = \BQuery::getInstance()->getFirstRow("SELECT value, type FROM options WHERE name = ?", [$name]);
            if (!empty($res) && !empty($res['value'])) {
                switch ($res['type']) {
                    case "S": return strval($res['value']);
                    default: return $res['value'];
                }
            } else {
                return null;
            }
        }

        public static function getOlympStartTime($ts = false) {
            $olymp_start = self::getOption("time_start");
            $timestamp = strtotime($olymp_start);
            return ($ts ? $timestamp : date(System::DT_FORMAT, $timestamp));
        }

        public static function getOlympEndTime($ts = false) {
            $olymp_start = self::getOption("time_end");
            $timestamp = strtotime($olymp_start);
            return ($ts ? $timestamp : date(System::DT_FORMAT, $timestamp));
        }

        public static function isOlympGoing() {
            return self::hasOlympStarted() && !self::hasOlympEnded();
        }

        public static function hasOlympStarted() {
            $ts = time();
            return ($ts >= self::getOlympStartTime(true));
        }

        public static function hasOlympEnded() {
            $ts = time();
            return ($ts > self::getOlympEndTime(true));
        }

        public static function diffTime($time1, $time2) {
            if (is_string($time1))
                $time1 = strtotime($time1);
            if (is_string($time2))
                $time2 = strtotime($time2);
            $dt = $time2 - $time1;
            $hours = floor($dt / 3600);
            $result = "";
            if ($hours > 0)
                $result .= ($hours < 10 ? "0$hours" : $hours) . ":";
            $dt -= $hours * 3600;
            $minutes = floor($dt / 60);
            $dt -= $minutes * 60;
            $result .= ($minutes < 10 ? "0$minutes" : $minutes) . ":" . ($dt < 10 ? "0$dt" : $dt);
            return $result;
        }

        public static function getTimeToEnd() {
            $ts = time();
            $olymp_end = self::getOlympEndTime(true);
            $dt = $olymp_end - $ts;
            $hours = floor($dt / 3600);
            $dt -= $hours * 3600;
            $minutes = floor($dt / 60);
            $dt -= $minutes * 60;
            $hours = ($hours < 10 ? "0$hours" : $hours);
            $minutes = ($minutes < 10 ? "0$minutes" : "$minutes");
            $dt = ($dt < 10 ? "0$dt" : "$dt");
            $result = "$hours:$minutes:$dt";
            return $result;
        }

        public static function maybeLock() {
            $db = BQuery::getInstance();
            $user = BUser::getInstance();
            $start_dt = date(System::DT_FORMAT, time() - 60);
            if ($user->logged())
                $cmds = $db->getFirstRow("SELECT COUNT(*) AS count FROM commands WHERE (ip = ? OR user_id = ?) AND dt >= ?",
                    [$_SERVER['REMOTE_ADDR'], $user->getID(), $start_dt]
                );
            else
                $cmds = $db->getFirstRow("SELECT COUNT(*) AS count FROM commands WHERE ip = ? AND dt >= ?", [
                    $_SERVER['REMOTE_ADDR'], $start_dt
                ]);
            if (!empty($cmds['count']) && $cmds['count'] > 20) {
                $db->insert("locks", [
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'user_id' => ($user->logged() ? $user->getID() : null),
                    'end_time' => date(System::DT_FORMAT, time() + 300)
                ]);
            }
        }

        public static function getTime() {
            return date(self::DT_FORMAT);
        }

        /**
         * @param      $answer
         *
         * @return array|int
         *
         * -1: Юзер незалогинен
         * -2: Не идет олимпиада
         * 0: Неверный ответ
         * 1: Верный ответ
         * 2: Верный ответ уже был дан
         */
        public static function answer($answer) {
            $user = BUser::getInstance();
            $db = BQuery::getInstance();
            if (empty($userID) && !$user->logged())
                return [ 'code' => -1, 'text' => "Залогиньтесь, прежде чем отвечать на задание." ];
            $userID = (empty($userID) ? $user->getID() : intval($userID));
            if (!self::isOlympGoing())
                return [ 'code' => -2, 'text' => "Олимпиада уже закончилась, либо еще не началась." ];
            $answerData = $db->getFirstRow("SELECT id, number, success_message FROM tasks WHERE answer_hash = ?", $answer);
            $insert = [
                'user_id' => $userID,
                'answer' => $answer,
                'dt' => self::getTime(),
                'status' => "F",
                'task_id' => (empty($answerData['id']) ? null : $answerData['id'])
            ];
            $res = 0;
            $defaultText = "Неверный ответ.";
            if (!empty($answerData['id'])) {
                $predAnswer = $db->getFirstRow("SELECT id FROM attempts WHERE user_id = ? AND answer = ?",
                    [ $user->getID(), $answer ]
                );
                $res = (!empty($predAnswer['id']) ? 2 : 1);
                $insert['status'] = ($res == 1 ? "S" : "M");
                $defaultText = ($res == 1 ? "Поздравляем! Одно из заданий решено." : "Вы уже решили это задание");
            }
            $db->insert("attempts", $insert);
            return [
                'number' => (empty($answerData['number']) ? null : $answerData['number']),
                'code' => $res,
                'text' => (empty($answerData['success_message']) || $res != 1) ? $defaultText : $answerData['success_message']
            ];
        }

        private static function updateTasksFromCSV($filename) {
            $db = BQuery::getInstance();
            $f = fopen($filename, "rt");
            if (!$f)
                return false;
            $i = 1;
            while (!feof($f)) {
                $item = fgetcsv($f);
                $data = [
                    'number' => $i,
                    'points' => $item[0],
                    'answer' => $item[1],
                    'success_message' => $item[2],
                    'answer_hash' => sha1($item[1])
                ];
                $exists = !empty($db->getFirstRow("SELECT id FROM tasks WHERE id = ?", $i));
                if ($exists)
                    $db->update("tasks", $data, "id = :id", ['id' => $i]);
                else
                    $db->insert("tasks", array_merge($data, ['id' => $i]));
                $i++;
            }
            fclose($f);
            return true;
        }

        public static function init() {
            if (!empty($_GET['mode']) && $_GET['mode'] == "update_users") {
                BUser::getInstance()->addFromCSV(dirname(__DIR__) . "/users.csv");
                die("Users updated.");
            } elseif (!empty($_GET['mode']) && $_GET['mode'] == "update_tasks") {
                self::updateTasksFromCSV(dirname(__DIR__) . "/tasks.csv");
                die("Tasks updated.");
            }
        }
    }