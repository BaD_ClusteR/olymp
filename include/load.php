<?php

    global $BC;

    spl_autoload_register(function($className) {
        $file = CLASSES_DIR . "/" . strtolower($className) . ".class.php";
        if (file_exists($file) && is_readable($file))
            include_once $file;
    });

    session_start();

    $BC['query'] = $bcdb = new BQuery();
    $BC['user'] = new BUser();
    System::init();
