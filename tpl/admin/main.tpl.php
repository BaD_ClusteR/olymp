<!DOCTYPE html>
<html lang="ru">
    <head>
        <title><?=empty($title) ? "Admin zone" : $title?></title>
        <script src="/tpl/assets/js/jquery-3.3.1.min.js"></script>
        <script src="/tpl/assets/js/admin.js"></script>
        <link rel="stylesheet" href="/tpl/assets/css/admin.css" />
    </head>
    <body>
        <div class="main">
            <ul class="admin__menu">
                <li class="admin__menu-item">
                    <a href="/admin" class="admin__menu-link">Users</a>
                </li>
                <li class="admin__menu-item">
                    <a href="/admin/commands" class="admin__menu-link">Commands log</a>
                </li>
                <li class="admin__menu-item">
                    <a href="/admin/attempts" class="admin__menu-link">Answer attempts log</a>
                </li>
                <li class="admin__menu-item">
                    <a href="/admin/logoff" class="admin__menu-link">Logoff</a>
                </li>
            </ul>
            <div class="content">
                <?php
                    function _render($vars, $content) {
                        foreach ($vars as $varName => $var) {
                            $$varName = $var;
                        }
                        include TPL_DIR . "/admin/" . $content . ".tpl.php";
                    }
                    _render($vars, $content);
                ?>
            </div>
        </div>
        <div class="terminal__wrapper">
            <div id="terminal"></div>
        </div>
    </body>
</html>