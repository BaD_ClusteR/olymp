<?php
    $table_fields = array_keys($columns);
?>
<h1><?=$title?></h1>
<table class="admin__table">
    <thead>
        <tr>
            <?php foreach ($table_fields as $field) :?>
                <th data-field="<?=$field?>"><?=$columns[$field]?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $row) :?>
            <tr<?php if (!empty($row['@class'])) :?> class="<?=$row['@class']?>"<?php endif;?><?php if (!empty($row['id'])) :?> data-id="<?=$row['id']?>"<?php endif; ?>>
                <?php foreach ($table_fields as $field) :?>
                    <td><?=$row[$field]?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>