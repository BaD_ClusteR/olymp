
let Log = function() {
    let maxStrings = 50;
    let tbody = document.querySelector(".admin__table tbody");
    let thead = document.querySelector(".admin__table thead");
    let maxID = -1;
    let fields = null;
    let self = this;
    let watchInterval = 2000;
    let interval = null;

    this.shorten = function() {
        if (tbody.childElementCount > maxStrings) {
            while (tbody.childElementCount > maxStrings)
                tbody.removeChild(tbody.lastElementChild);
        }
    };

    this.add = function(data) {
        if (!fields)
            fields = getFields();
        let cellCount = fields.length;
        let tr = document.createElement("TR");
        if (typeof data.id !== "undefined")
            tr.setAttribute("data-id", data.id);
        if (typeof data['@class'] !== "undefined")
            tr.setAttribute("class", data['@class']);
        for (let i = 0; i < cellCount; i++) {
            let td = document.createElement("TD");
            td.innerHTML = (typeof data[fields[i]] !== "undefined" ? data[fields[i]].toString() : "");
            tr.appendChild(td);
        }
        tr.classList.add("new");
        tbody.insertBefore(tr, tbody.firstElementChild);
        setTimeout(function() {
            tr.classList.remove("new");
        }, 300);
        if (tbody.childElementCount > maxStrings)
            this.shorten();
    };

    this.getMaxStringsCount = function() {
        return maxStrings;
    };

    this.setMaxStringsCount = function(stringsCount) {
        maxStrings = stringsCount;
        this.shorten();
    };

    let findMaxID = function() {
        tbody.querySelectorAll("tr").forEach(function(tr) {
            let id = tr.getAttribute("data-id");
            if (id)
                maxID = Math.max(parseInt(id), maxID);
        });
    };

    let getFields = function() {
        let res = [];
        thead.querySelectorAll("th").forEach(function(th) {
            res[res.length] = th.getAttribute("data-field");
        });
        return res;
    };

    this.update = function() {
        let mode = window.location.pathname.split("/")[2];
        if (maxID < 0)
            findMaxID();
        if (!fields)
            fields = getFields();
        jQuery.ajax("/ajax/" + mode + "/" + maxID, {
            type: "GET",
            dataType: "json",
            success: function(data) {
                data.forEach(function(row) {
                    self.add(row);
                    if (typeof row.id !== "undefined" && row.id > maxID)
                        maxID = row.id;
                });
            }
        });
    };

    this.watch = function() {
        if (interval)
            clearInterval(interval);
        interval = setInterval(self.update, watchInterval);
    };

    let mode = window.location.pathname.split("/")[2];
    if (["attempts", "commands"].indexOf(mode) !== -1)
        this.watch();
};

let SysUtils = function() {
    if (document.querySelector(".admin__table"))
        this.logHandler = new Log();
};

document.addEventListener("DOMContentLoaded", function() {
    BC = new SysUtils();
});