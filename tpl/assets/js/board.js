// Dimensions of sunburst.
let rect = document.querySelector(".legend").getBoundingClientRect();
var width = rect.width;
var height = rect.height;
var radius = Math.min(width, height) / 2;
var lastID = null;

// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
var b = {
    w: 75, h: 30, s: 3, t: 10
};

// Mapping of step names to colors.
var colors = {
    "home": "#5687d1",
    "product": "#7b615c",
    "search": "#de783b",
    "account": "#6ab975",
    "other": "#a173d1",
    "end": "#bbbbbb"
};

// Total size of all segments; we set this later, after loading the data.
var totalSize = 0;

var vis = d3.select("#chart").append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .append("svg:g")
    .attr("id", "container")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var partition = d3.partition()
    .size([2 * Math.PI, radius * radius]);

var arc = d3.arc()
    .startAngle(function(d) { return d.x0; })
    .endAngle(function(d) { return d.x1; })
    .innerRadius(function(d) { return Math.sqrt(d.y0); })
    .outerRadius(function(d) { return Math.sqrt(d.y1); });
var loaded = false;

d3.json("/ajax/board")
    .then(function(json) {
        createVisualization(json);
        loaded = true;
    });

function createVisualization(json) {

    vis.append("svg:circle")
        .attr("r", radius)
        .style("opacity", 0);

    var root = d3.hierarchy(json)
        .sum(function(d) { return d.size; })
        .sort(function(a, b) { return b.value - a.value; });

    // For efficiency, filter nodes to keep only those large enough to see.
    var nodes = partition(root).descendants()
        .filter(function(d) {
            return (d.x1 - d.x0 > 0.005); // 0.005 radians = 0.29 degrees
        });

    var path = vis.data([json]).selectAll("path")
        .data(nodes)
        .enter().append("svg:path")
        .attr("display", function(d) { return d.depth ? null : "none"; })
        .attr("d", arc)
        .attr("fill-rule", "evenodd")
        .attr("class", (d) => (`u${d.data.user} t${d.data.task} ` + (d.data.solved ? "solved" : "not-solved") + (typeof myid !== "undefined" && myid === d.data.user ? " current" : "")))
        .attr("data-user", (d) => console.log(d))
        /*.attr("data-task", (d) => d.data.task)*/
        .on("mouseover", mouseover)
        .on("mouseout", function() {
            d3.selectAll("path").classed("selected", false).classed("not-selected", false);
            $(".label.active").removeClass("active");
        });

    // Add the mouseleave handler to the bounding circle.
    d3.select("#container").on("mouseleave", mouseleave);

    // Get total size of the tree = value of root node from partition.
    totalSize = path.datum().value;
}

function mouseover(d) {
    if ($("#chart svg").hasClass("interactive")) {
        var sequenceArray = d.ancestors().reverse();
        //let user = d.attr("data-user");
        d.descendants().forEach(function (dd) {
            sequenceArray[sequenceArray.length] = dd;
        });
        sequenceArray.shift(); // remove root node from the array

        d3.selectAll("path")
            .classed("not-selected", true)
            .classed("selected", false);

        vis.selectAll("path")
            .filter(function (node) {
                return (sequenceArray.indexOf(node) >= 0);
            })
            .classed("selected", true);
        $(".label.active").removeClass("active");
        $(`.label.l-u${d.data.user}`).addClass("active");
    }
}

// Restore everything to full opacity when moving off the visualization.
function mouseleave(d) {
    d3.selectAll("path")
        .classed("selected", false)
        .classed("not-selected", false);
}

function update() {
    $.ajax("/ajax/answers" + (!lastID ? "" : "/" + parseInt(lastID)), {
        dataType: "json",
        success: function(data) {
            data.forEach(function(item) {
                $("#chart .u" + item.user + ".t" + item.number).removeClass("not-solved").addClass("solved");
                lastID = Math.max(lastID, item.id);
            });
        }
    })
}

function update_table() {
    $.ajax("/ajax/board_table", {
        dataType: "json",
        success: function(data) {
            let canvas = $("#table tbody");
            let html = "";
            data.forEach(function(user, index) {
                let className = user.current ? "class='current'" : "";
                html += `<tr ${className}><td>${index + 1}</td><td>${user.name}</td><td>${user.solved}</td><td>${user.points}</td></tr>`;
            });
            canvas.html(html);
        }
    });
}

function place_labels() {
    let r = width / 2 + 20;
    let decimals = 6;
    let labels = document.querySelectorAll(".label");
    let count = labels.length;
    let firstHalf = Math.round(count / 2);
    let angle = 360 / count;
    labels.forEach(function(label, i) {
        if (typeof i !== "undefined") {
            let currAngle = angle * i + (angle / 2);
            currAngle = currAngle / 180 * Math.PI;
            let sin = Math.sin(currAngle).toFixed(decimals);
            let cos = Math.cos(currAngle).toFixed(decimals);
            let dx = (i < firstHalf ? 0 : -label.getBoundingClientRect().width);
            let dy = -Math.round(label.getBoundingClientRect().height / 2);
            let transform = `translate3d(${Math.round(sin * r) + dx}px, ${Math.round(-cos * r) + dy}px, 0)`;
            if (i < firstHalf)
                transform += ` rotateZ(${-5 + i}deg)`;
            else
                transform += ` rotateZ(${-5 + i - firstHalf}deg)`;
            label.style.transform = transform;
        }
    });
}

window.addEventListener("load", function() {
    let svg = $("#chart svg");
    place_labels();
    let chart = document.getElementById("chart");
    chart.style.width = `${width}px`;
    chart.style.height = `${height}px`;
    let interval = setInterval(function() {
        if (loaded) {
            svg.addClass("active");
            svg.parent().addClass("active");
            setTimeout(function() {
                svg.addClass("interactive");
                setInterval(function() {
                    update();
                    update_table();
                }, 10000);
            }, 1500);
            clearInterval(interval);
        }
    }, 300);

});