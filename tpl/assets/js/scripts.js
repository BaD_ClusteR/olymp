/*let Console = function() {
    let dom = {
        body: document.body,
        field: document.querySelector(".console__input-textarea"),
        cmdLine: document.querySelector(".console__input-cmd"),
        caret: document.querySelector(".console__input-cursor"),
        console: document.querySelector(".console"),
        history: document.querySelector(".console__history"),
        prefix: document.querySelector(".console__input-prefix")
    };
    let self = this;
    let enabled = false;

    let setFocus = function() {
        if (dom.field && document.activeElement !== dom.field)
            dom.field.focus();
    };

    let escapeChar = function(c) {
        switch (c) {
            case " ": return "&nbsp;";
            case "\n": return "";
            default: return c;
        }
    };

    let enableConsole = function() {
        enabled = true;
        dom.console.classList.add("active");
    };

    let disableConsole = function() {
        enabled = false;
        setTimeout(function() {
            if (!enabled)
                dom.console.classList.remove("active");
        }, 100);
    };

    let sendCommand = function(cmd) {
        if (arguments.length === 0)
            cmd = dom.field.value;
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/cmd');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send("cmd=" + decodeURIComponent(cmd));

        if (xhr.status !== 200) {

        } else {
            console.log(xhr.statusText);
        }
    };

    let addHistory = function(cmd, withPrefix) {
        let text = cmd;
        if (withPrefix)
            text = dom.prefix.innerHTML + " " + text;
        let item = document.createElement("SPAN");
        item.innerHTML = text;
        dom.history.appendChild(item);
    };

    this.setCommand = function(cmd) {
        if (arguments.length === 0)
            cmd = dom.field.value;
        dom.cmdLine.querySelectorAll("span:not(.console__input-cursor)").forEach(function(span) {
            dom.cmdLine.removeChild(span);
        });
        for (let i = 0; i < cmd.length; i++) {
            let span = document.createElement("SPAN");
            span.innerHTML = escapeChar(cmd.charAt(i));
            dom.cmdLine.insertBefore(span, dom.caret);
        }
        dom.console.setAttribute("style", "--console-block-height:" + dom.cmdLine.getBoundingClientRect().height + "px");
    };

    this.exec = function(cmd) {
        if (arguments.length === 0)
            cmd = dom.field.value;
        addHistory(cmd, true);
        sendCommand(cmd);
    };

    [ "click", "focus", "click", "mousedown", "mouseup", "keydown", "keyup", "keypress" ].forEach(function(event) {
        dom.body.addEventListener(event, setFocus);
    });

    dom.field.addEventListener("keyup", function(e) {
        if (enabled) {
            if (e.key === "Enter") {
                self.exec();
                dom.field.value = "";
                self.setCommand("");
            }
            self.setCommand();
        }
    });
    dom.field.addEventListener("change", function() { if (enabled) self.setCommand(); });
    dom.field.addEventListener("focus", enableConsole);
    dom.field.addEventListener("blur", disableConsole);
};*/

let SysUtils = function() {
    let self = this;
    this.cls = false;

    this.terminal = $('#terminal').terminal(function(command) {}, {
        greetings: /*GLOBALS.text*/"",
        name: 'terminal',
        height: 200,
        prompt: GLOBALS.prompt,
        convertLinks: false,
        onBeforeCommand: function(command) {
            self.cls = (
                ["cls", "status"].indexOf(command) !== -1 ||
                ["cls ", "cls\t", "status ", "status\t"].indexOf(command.substr(0, 4)) !== -1
            );
            if (self.cls)
                BC.terminal.settings().echoCommand = false;
            if (command !== "") {
                $.ajax("/cmd", {
                    type: "POST",
                    dataType: "json",
                    data: {
                        cmd: command
                    },
                    success: function(result) {
                        if (result.status === "success") {
                            result.cmds.forEach(function(cmd) {
                                self.parser.exec(cmd);
                            });
                        }
                    }
                });
            }
        },
        onAfterCommand: function(cmd) {
            if (self.cls) {
                self.cls = false;
                BC.terminal.settings().echoCommand = true;
            }
        },
        onCommandChange: function(command, terminal) {
            if (command.length > 100)
                terminal.set_command(command.substr(0, 100));
        }
    });

    this.parser = new ConsoleInterpreter(this.terminal);

    $.ajax("/ajax/init", {
        type: "GET",
        dataType: "json",
        success: function(result) {
            result.cmds.forEach(function(cmd) {
                self.parser.exec(cmd);
            });
        }
    });

};

let ConsoleInterpreter = function(terminal) {

    let echo = function(args) {
        let result = "";
        args.forEach(function(arg) {
            result += arg;
        });
        //result = result.replace("[", "&#91;").replace("]", "&#93;");
        return terminal.echo(result);
    };

    let setPrompt = function(args) {
        if (args.length === 0)
            args = [""];
        return terminal.set_prompt(args[0]);
    };

    let redirect = function(args) {
        if (args.length === 0)
            return;
        window.location = args[0];
        return true;
    };

    let cls = function() {
        BC.terminal.clear();
        return true;
    };

    let open = function(args) {
        window.open(args.length === 0 ? "" : args[0]);
        return true;
    };

    let setMask = function(args) {
        BC.terminal.set_mask(args.length === 0 ? false : args[0]);
        return true;
    };

    this.exec = function(cmd) {
        let args = [];
        for (let i = 1; i < cmd.length; i++)
            args[args.length] = cmd[i];
        switch (cmd[0]) {
            case "echo": return echo(args);
            case "setPrompt": return setPrompt(args);
            case "setMask": return setMask(args);
            case "redirect": return redirect(args);
            case "cls": return cls();
            case "open": return open(args);
        }
        return null;
    }
};

document.addEventListener("DOMContentLoaded", function() {
    window.BC = new SysUtils();
});