<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Dashboard</title>
        <link rel="stylesheet" href="<?=$BC['dirs']['css']?>/board.css" />
        <script src="<?=$BC['dirs']['js']?>/jquery-3.3.1.min.js"></script>
        <script src="<?=$BC['dirs']['js']?>/d3.min.js"></script>
        <?php if (BUser::getInstance()->logged()) :?><script>let myid=<?=BUser::getInstance()->getID()?></script><?php endif;?>
    </head>
    <body>
        <div id="main">
            <div id="chart"></div>
            <div class="legend">
                <?php foreach ($info as $index => $user) :?>
                    <div class="label l<?=$index + 1?> l-u<?=$user['id']?><?=BUser::getInstance()->logged() && BUser::getInstance()->getID() == $user['id'] ? " current" : "" ?>"><?=$user['surname']?> <?=$user['name']?><?=(!empty($user['patronym']) ? " $user[patronym]" : "")?></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="table">
            <table>
                <thead>
                    <tr>
                        <th>Место</th>
                        <th>ФИО</th>
                        <th>Флаги</th>
                        <th>Очки</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($table as $index => $tr) :?>
                        <tr<?=$tr['current'] ? " class='current'" : ""?>>
                            <td><?=$index + 1?></td>
                            <td><?=$tr['name']?></td>
                            <td><?=$tr['solved']?></td>
                            <td><?=$tr['points']?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <script src="<?=$BC['dirs']['js']?>/board.js"></script>
    </body>
</html>