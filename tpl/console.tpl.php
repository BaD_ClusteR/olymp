<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Console</title>
        <link rel="stylesheet" href="/tpl/assets/css/terminal.css" />
        <script src="/tpl/assets/js/jquery-3.3.1.min.js"></script>
        <script src="/tpl/assets/js/jquery.terminal-2.3.0.min.js"></script>
        <script>let GLOBALS = <?=json_encode($init)?></script>
        <script src="/tpl/assets/js/scripts.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700&amp;subset=cyrillic-ext,greek-ext,latin-ext" rel="stylesheet">
    </head>
    <body>
        <div class="terminal__wrapper">
            <div id="terminal"></div>
        </div>
    </body>
</html>